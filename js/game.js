//RequestAnimFrame: a browser API for getting smooth animations
window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame   ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			window.oRequestAnimationFrame      ||
			window.msRequestAnimationFrame     ||
		function( callback ){
			return window.setTimeout(callback, 1000 / 60);
		};
})();

window.cancelRequestAnimFrame = ( function() {
	return  window.cancelAnimationFrame          ||
			window.webkitCancelRequestAnimationFrame ||
			window.mozCancelRequestAnimationFrame    ||
			window.oCancelRequestAnimationFrame      ||
			window.msCancelRequestAnimationFrame     ||
		clearTimeout;
} )();
//---------------------------------DO NOT TOUCH CODE ABOVE


//console.log('Holla');


//step 01 - zw - create game canvas and track mouse position
var gameCanvas = document.getElementById("canvas"); //store HTML5 canvas tag into JS variable
var ctx = gameCanvas.getContext("2d"); //create context 2D
var W = window.innerWidth;
var H = window.innerHeight;
var mouseObj = {};

//console.log('browser width is currently:' + W)
//console.log('browser height is currently:' + H)

gameCanvas.width = W;
gameCanvas.height = H;


//step 02 - zw - clear page canvas by covering it in black
function paintCanvas(){
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, W, H);
}
paintCanvas();


function trackPosition(evt){
    mouseObj.x = evt.pageX;
    mouseObj.y = evt.pageY;
    //console.log("cursor x is: " + mouseObj.x + "   " + "cursor y is: " + mouseObj.y);
}
gameCanvas.addEventListener("mousemove", trackPosition, true);


//step 03 - zw - place a ball on canvas
var ball = {}; //ball object
ball = {
    x: 50,
    y: 50,
    r: 5,
    c: "#ffffff",
    vx: 4,
    vy: 8,
    
    draw: function(){
        ctx.beginPath();
        ctx.fillStyle = this.c;
        ctx.arc(this.x, this.y, this.r, 0, Math.PI*2, false);
        ctx.fill();
    }
}
ball.draw();


//step 04 - zw - place a start button on canvas
var startBtn = {}; //start button object
startBtn = {
	w: 100,
	h: 50,
	x: W/2-50,
	y: H/2-25,
	
	draw: function(){
		ctx.strokeStyle = "#ffffff";
		ctx.lineWidth = "2";
		ctx.strokeRect(this.x, this.y, this.w, this.h);
		
		ctx.font = "18px Arial, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#ffffff";
		ctx.fillText("START", W/2, H/2);
	}
}
startBtn.draw();


//step 05 - zw - place score and points on canvas
var points = 0; //game points
function paintScore(){
	ctx.fillStyle = "#ffffff";
	ctx.font = "18px Arial, sans-serif";
	ctx.textAlign = "left";
	ctx.textBaseline = "top";
	ctx.fillText("Score: " + points, 20, 20);
}
paintScore();


//step 06 - zw - place paddles(top and bottom) on canvas
function paddlePosition(TB){
    this.w = 150;
    this.h = 5;
    
    this.x = W/2-this.w/2; //linking center of paddle to center of canvas - width wise
    
    //setting up paddles at top or bottom of canvas
    if(TB == "top"){
        this.y = 0;
    }else{
        this.y = H-this.h;
    }
    
    //this.y = (TB == "top") ? 0 : H-this.h; <-- shortened if/else
}

var paddlesArray = []; //paddles array
paddlesArray.push(new paddlePosition("top"));
paddlesArray.push(new paddlePosition("bottom"));
//console.log("top paddle y is: " + paddlesArray[0].y);
//console.log("bottom paddle y is: " + paddlesArray[1].y);

function paintPaddles(){
    for(var lp = 0; lp < paddlesArray.length; lp++){
        p = paddlesArray[lp];
        
        ctx.fillStyle = "#ffffff";
        ctx.fillRect(p.x, p.y, p.w, p.h);
    }
}
paintPaddles();


//step 07 - zw - detect when user clicks on screen
gameCanvas.addEventListener("mousedown", btnClick, true);

function btnClick(evt){
    var mx = evt.pageX;
    var my = evt.pageY;
    
    //determines if user clicked on start button
    if(mx >= startBtn.x && mx <= startBtn.x+startBtn.w){
        if(my >= startBtn.y && my <= startBtn.y+startBtn.h){
            //console.log("ya clicked on the start button, congrats");
            
            //delete the start button
            startBtn = {};
            
            //start game animation loop
            animloop();
        }
    }
    //determines if user clicked on restart button
    if(flagGameOver == 1){
        if(mx >= restartBtn.x && mx <= restartBtn.x+restartBtn.w){
            if(my >= restartBtn.y && my <= restartBtn.y+restartBtn.h){
                //reset game
                points = 0;
                ball.x = 20;
                ball.y = 20;
                ball.vx = 4;
                ball.vy = 8;
                flagGameOver = 0;
                
                animloop(); //start game animation loop
            }
        }
    }
}

//function for running whole game animation
var init; //variable to initialize animation
function animloop(){
    init = requestAnimFrame(animloop);
    refreshCanvasFun();
}


//step 08 - zw - draw everything on canvas over and over
function refreshCanvasFun(){
    paintCanvas();
    paintPaddles();
    ball.draw();
    paintScore();
    update();
}

function update(){
    //move paddles - track mouse
    for(var lp = 0; lp < paddlesArray.length; lp++){
        p = paddlesArray[lp];
        p.x = mouseObj.x - p.w/2; //paddle centered with mouse position
    }
    
    //move ball
    ball.x += ball.vx;
    ball.y += ball.vy;

    check4collision(); //check for ball/paddle collision
}

function check4collision(){
    var pTop = paddlesArray[0];
    var pBot = paddlesArray[1];
    
    if(collides(ball, pTop)){
        collideAction(ball, pTop);
    }else if(collides(ball, pBot)){
        collideAction(ball, pBot);
    }else{
        //when ball goes out from top/bottom of screen
        if(ball.y + ball.r > H){
            gameOver();
        }else if(ball.y < 0){
            gameOver();
        }
        //ball hits sides of screen
        if(ball.x + ball.r > W){
            ball.vx = -ball.vx;
            ball.x = W-ball.r;
        }else if(ball.x - ball.r < 0){
            ball.vx = -ball.vx;
            ball.x = ball.r;
        }
    }
    //sparkles time
    if(flagCollision == 1){
        for(var k = 0; k < particleCount; k++){
            particles.push(new createParticles(particlePos.x, particlePos.y, particleDir));
        }
    }
    emitParticles(); //emit particles/sparks
    flagCollision = 0; //reset of flagCollision
}

//sparkles time - functions
function createParticles(x, y, d){
    this.x = x || 0;
    this.y = y || 0;
    this.radius = 2;
    this.vx = -1.5+Math.random()*3;
    this.vy = d*Math.random()*1.5;
}
function emitParticles(){
    for(var j = 0; j < particles.length; j++){
        par = particles[j];
        
        ctx.beginPath();
        ctx.fillStyle = "#ffffff";
        if(par.radius > 0){
            ctx.arc(par.x, par.y, par.radius, 0, Math.PI*2, false);
        }
        ctx.fill();
        par.x += par.vx;
        par.y += par.vy;
        
        //reduce radius of particle so that it dies after a few seconds
        par.radius = Math.max(par.radius-0.05, 0.0);
    }
}

var paddleHit; //which paddle was hit -- top=0, bottom=1
function collides(b, p){
    if(b.x+b.r >= p.x && b.x-b.r <= p.x+p.w){
        if(b.y >= (p.y-p.h) && p.y > 0){
            paddleHit = 0;
            return true;
        }else if(b.y <= p.h && p.y === 0){
            paddleHit = 1;
            return true;
        }else{
            return false;
        }
    }
}

var collisionSnd = document.getElementById("collide"); //audio obj

function collideAction(b, p){
    //console.log("sound ---> bounce")
    if(collisionSnd){
        collisionSnd.play(); //play sound when ball hits paddles
    }
    ball.vy = -ball.vy; //reverse ball y velocity
    
    if(paddleHit == 0){
        //ball hit top paddle
        ball.y = p.y-p.h;
        particlePos.y = ball.y+ball.r;
        particleDir = -1;
    }else if(paddleHit == 1){
        //ball hit bottom paddle
        ball.y = p.h+ball.r;
        particlePos.y = ball.y-ball.r;
        particleDir = 1;
    }
    points++; //increase score by 1
    increaseSpd(); //increase speed of ball at certain point values
    particlePos.x = ball.x; //sparkly things appear after ball hits paddles
    
    flagCollision = 1;
}

//sparkles time - global vars
var flagCollision = 0; //flag var for when ball collides with paddle for particles
var particles = []; //array for particles
var particlePos = {} //object to contain position of collision
var particleDir = 1; //var to control direction of particles
var particleCount = 20; //amount of particles when ball collides with paddles


function increaseSpd(){
    //increase ball speed after every 4 points
    if(points%4 === 0){
        if(Math.abs(ball.vx) < 15){
            ball.vx += (ball.vx < 0) ? -1 : 1;
            ball.vy += (ball.vy < 0) ? -2 : 2;
        }
    }
}

var flagGameOver = 0; //global var for determining if game is over or not
//function to run when game is over
function gameOver(){
    //console.log("ya loser");
    
    //display final score
    ctx.fillStyle = "#ffffff";
    ctx.font = "20px Arial, sans-serif";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("Game Over - You scored " + points + " points!", W/2, H/2+25);
    
    cancelRequestAnimFrame(init); //stop the animation
    restartBtn.draw(); //display replay button
    
    flagGameOver = 1; //set game over flag - track the change (this case -> game over)
}

var restartBtn = {}; //restart button object
restartBtn = {
	w: 100,
	h: 50,
	x: W/2-50,
	y: H/2-50,
	
	draw: function(){
		ctx.strokeStyle = "#ffffff";
		ctx.lineWidth = "2";
		ctx.strokeRect(this.x, this.y, this.w, this.h);
		
		ctx.font = "18px Arial, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#ffffff";
		ctx.fillText("REPLAY", W/2, H/2-25);
	}
}